from setuptools import find_packages, setup
import sys

_ver = "0.1.2"
_is_building = (sys.argv[1] == "bdist_wheel")

setup(
    name = "uzaklib",
    packages = find_packages(exclude = ["venv"]),
    version = _ver,
    description = "Uzak's QOL Python library",
    author = "UZAK",
    license = "MIT",
    install_requires = ["matplotlib", "numpy"],
    setup_requires = ["pytest-runner"],
    tests_require = ["pytest"],
    test_suite = "tests"
)

if _is_building:
    with open("job.env", "wt") as env_file:
        env_file.write("BUILD_VERSION=" + _ver + "\n")
