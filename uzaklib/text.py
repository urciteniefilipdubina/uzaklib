_WRONG_TYPE_MESSAGE = "Input type doesn't match any of the allowed types!"

def to_str(i) -> str:
    """Takes either a list of (int)s in range(128) or a
    bytesequence (bytes) and converts it into a string.
    Arguments:
        i -> list of (int)s in range(128) or bytes"""

    if isinstance(i, list):
        return to_str(to_bytes(i))
    elif isinstance(i, bytes):
        return i.decode(encoding = "ascii")
    else:
        raise ValueError(_WRONG_TYPE_MESSAGE)

def to_bytes(i) -> bytes:
    """Takes either a list of (int)s in range(128)
    or a string and converts it into a bytesequence.
    Arguments:
        i -> list of (int)s in range(128) or str"""

    if isinstance(i, str):
        return bytes(i, encoding = "ascii")
    elif isinstance(i, list):
        return bytes(i)
    else:
        raise ValueError(_WRONG_TYPE_MESSAGE)

def to_bin(i) -> list:
    """Takes either a bytesequence (bytes) or a string
    and converts it into a list of (int)s in range(128).
    Arguments:
        i -> bytes or str"""
    
    if isinstance(i, str):
        return to_bin(to_bytes(i))
    elif isinstance(i, bytes):
        return list(i)
    else:
        raise ValueError(_WRONG_TYPE_MESSAGE)
