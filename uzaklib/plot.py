import matplotlib.pyplot
import numpy

from .file import writej, readj

class plot:
    """Plot data holder class."""

    def __init__(this, x: list, y: list) -> None:
        this.x = x
        this.y = y
        this.show()
    def __repr__(this) -> str:
        this.show()
        return "plot object"
    def show(this) -> None:
        """Shows the plot graphically."""

        matplotlib.pyplot.plot(numpy.array(this.x), numpy.array(this.y))
        matplotlib.pyplot.show()
    def write(this, file_name: str) -> int:
        """Writes the plot into a JSON file.
        Return size of the file in bytes.
        Arguments:
            file_name -> file name"""

        this.compound = {}
        for i in range(len(this.x)):
            this.compound[str(this.x[i])] = this.y[i]
        return writej(file_name, this.compound)

def read(file_name: str) -> plot:
    """Reads a plot from a JSON file and returns it as a plot object.
    Arguments:
        file_name -> file name"""

    compound = readj(file_name)
    x, y = list(compound.keys()), list(compound.values())
    for i in range(len(x)):
        x[i] = int(x[i])
    return plot(x, y)
