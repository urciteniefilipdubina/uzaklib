import json

def writes(f: str, s: str = "") -> int:
    """Writes a string into a file.
    Returns size of the file in bytes.
    Arguments:
        f -> file name
        s -> string to write"""

    with open(f, "wt") as fi:
        size = fi.write(s)
    return size

def reads(f: str) -> str:
    """Reads a file and returns it as a string.
    Arguments:
        f -> file name"""

    with open(f, "rt") as fi:
        s = fi.read()
    return s

def writeb(f: str, b: bytes = b"") -> int:
    """Writes a raw bytesequence into a file.
    Returns size of the file in bytes.
    Arguments:
        f -> file name
        b -> bytesequence to write"""

    with open(f, "wb") as fi:
        size = fi.write(b)
    return size

def readb(f: str) -> bytes:
    """Reads a file and returns it as a raw bytesequence
    Arguments:
        f -> file name"""

    with open(f, "rb") as fi:
        b = fi.read()
    return b

def writej(f: str, j: dict = {}) -> int:
    """Writes a dictionary into a JSON file.
    Returns size of the file in bytes.
    Arguments:
        f -> file name
        j -> dictionary to write"""

    s = list(json.dumps(j, separators = (",\n\t", ": ")))
    s.insert(1, "\n")
    s.insert(2, "\t")
    s.insert(len(s) - 1, "\n")
    return writes(f, "".join(s))

def readj(f: str) -> dict:
    """Reads a JSON file and returns it as a dictionary
    Arguments:
        f -> file name"""

    return json.loads(reads(f))
