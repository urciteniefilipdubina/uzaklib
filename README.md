# Uzak's QOL Python library

## CONTENTS
* [IMPORTANT NOTES](#important-notes)
* [INSTALLATION GUIDE](#installation-guide)
* [FEATURE OVERVIEW](#feature-overview)

## IMPORTANT NOTES [↑](#contents)

* This library is not on PyPI
* [Certain parts](#these-parts-of-the-library-are-not-tested) of this library are not tested, therefore they may not work in some cases
* This library requires [certain other libraries](#this-library-requires-these-libraries-from-pypi-to-work) from PyPI to work

### These parts of the library are not tested:
* Plotting tools
* File tools

### This library requires these libraries from PyPI to work:
* matplotlib
* numpy

## INSTALLATION GUIDE [↑](#contents)
1. Download the artifact of the pipeline from the latest tag
2. Unzip the archive
3. Install the library with pip from the wheel file

## FEATURE OVERVIEW [↑](#contents)
* The `file` submudule
    * `writes` and `reads` functions for working with ASCII-convertible files
    * `writeb` and `readb` functions for working with raw binary files
    * `writej` and `readj` functions for working with JSON files
* The `plot` submodule
    * `plot` class holding the plot data
        * `plot.show` method that shows the plot graphically
        * `plot.write` method that writes the plot into a JSON file
    * `read` function that reads a plot from a JSON file
* The `text` submodule
    * This submodule work with 3 datatypes:
        * `list` of `int`s in range(128)
        * `bytes`
        * `str`
    * `to_str` function that converts the other two types into `str`
    * `to_bytes` function that converts the other two types into `bytes`
    * `to_bin` function that converts the other two types into `list` of `int`s in range(128)
