from uzaklib import text

def test_text() -> None:
    s = "UZAK is an awesome person 123"
    by = bytes(s, encoding = "ascii")
    bi = list(by)

    assert text.to_bin(s) == bi
    assert text.to_bin(by) == bi
    assert text.to_bytes(s) == by
    assert text.to_bytes(bi) == by
    assert text.to_str(bi) == s
    assert text.to_str(by) == s
